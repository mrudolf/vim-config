"setlocal makeprg=php\ -l\ %
"setlocal errorformat=%m\ in\ %f\ on\ line\ %l,%-GErrors\ parsing\ %f,%-G
"nnoremap <buffer> <silent> <f5> :update<bar>sil! make<bar>cwindow<cr>

compiler php
setlocal makeprg=php\ -l\ %
setlocal grepprg=ag\ --vimgrep\ --php\ --html\ $*
map <D-¨> :!publish<CR>
imap <D-¨> <ESC>:!publish<CR>i

let php_baselib = 1
let php_noShortTags = 1
let php_parent_error_close = 1
let php_parent_error_open = 1
let php_htmlInStrings = 1

" Folding disabled as it is slow for some files
" let php_folding = 1
" set foldlevelstart=1
" set foldlevel=1

let b:api_search = "php"
