if !exists('loaded_snippet') || &cp
	finish
endif

Snippet < <<{tag}>><{}></<{tag}>>
Snippet div <div><CR><{}><CR></div>
Snippet ul <ul><CR><li><{}></li><CR></ul>
Snippet ol <ol><CR><li><{}></li><CR></ol>
Snippet li <li><{}></li>
Snippet js <script src="/scripts/<{script}>.js\"></script><CR>
Snippet fa <i class="fa fa-<{icon}>">&zwj;</i><{}>
Snippet a <a href="<{link}>"><{}></a>
Snippet mail <a href="mailto:<{mail}>"><{mail}></a><{}>
Snippet form <form method="post" action="<{action}>"><CR><{}><CR></form><CR>
Snippet select <select name="<{name}>"><CR><option value=""></option><CR><{}><CR></select><CR>
Snippet option <option value="<{value}>"><{value}><{}></option>
Snippet input <input type="<{text}>" name="<{name}>">
Snippet form <form action="/actions/<{update}>" method="POST"><CR></form><CR>
Snippet doctype <!doctype html><CR><html><CR><head><CR><meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1"><CR><meta http-equiv="content-type" content="text/html; charset=utf-8"><CR><title><{}></title><CR></head><CR><body><CR></body><CR></html><CR>
Snippet script <script src="<{}>"></script><CR>
Snippet css <link rel="stylesheet" href="<{}>.css" type="text/css">
