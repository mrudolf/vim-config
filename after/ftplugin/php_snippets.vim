if !exists('loaded_snippet') || &cp
	finish
endif

Snippet <? <?php<CR><{}>?>
Snippet class class <{MyClass}><CR>{<CR><CR>}<CR>
Snippet req require_once '<{file}>.php';<CR><{}>
Snippet inc include_once '<{file}>.php';<CR><{}>
Snippet if if (<{condition>}>) {<CR><{}><CR>}<CR>
Snippet foreach foreach ($<{items}> as $<{item}>) {<CR><{}><CR>}
Snippet for for ($<{i}> = 0; $<{i}> < <{max}>; $<{i}>++)<CR><{}>
Snippet error error_log("<{var}>: " . print_r($<{var}>, true));
Snippet iss isset($<{var}>) ? $<{var}> : '';
Snippet header header('location:/<{index}>');<CR>
Snippet vendor require_once('vendor/autoload.php');<CR>
Snippet date date('Y-m-d')


