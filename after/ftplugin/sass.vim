setlocal grepprg=ag\ --vimgrep\ --sass\ $*
setlocal makeprg=sass\ --style=compressed\ --sourcemap=none\ --update\ %:p:h
let b:api_search = "css"
