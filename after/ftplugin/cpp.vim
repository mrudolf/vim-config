setlocal grepprg=ag\ --vimgrep\ --cpp\ $*
let b:api_search = "cpp"

if !exists('*SwitchHeaderSource')
	function! SwitchHeaderSource()
		if (expand ("%:e") == "cpp")
			find %:t:r.h
		else
			find %:t:r.cpp
		endif
	endfunction
endif

nnoremap <leader>s :call SwitchHeaderSource()<CR>
