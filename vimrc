" Pathogen
execute pathogen#infect()

set history=698
filetype plugin on
filetype indent on

set autoread
set ignorecase
set smartcase
set gdefault
set incsearch
set showmatch
set magic

syntax enable
set encoding=utf8
set nobackup
set autowrite
set modelines=0
set nocompatible

set smarttab
set tabstop=3
set shiftwidth=3
set hidden

set cryptmethod=blowfish

if has("gui_running")
	colorscheme distinguished
	if has('mac')
		set guifont=Monaco:h14
	else
		set guifont=Monaco\ 10
	endif
	set lines=40
	set guioptions+=e
	set guioptions-=T
	set showtabline=2
else
	if has("unix")
		let s:uname = system("uname")
		if s:uname == "Darwin\n"
			colorscheme hemisu
		else
			colorscheme grb256
		endif
	else
		colorscheme grb256
	endif
endif

set ruler
set showmode
set visualbell
set showcmd
set linebreak

set wildmenu
set wildmode=list:longest

set spelllang=pl

set tags=.tags

set cryptmethod=blowfish2

" Use ag as grep
set grepprg=ag\ --vimgrep\ $*
set grepformat=%f:%l:%c:%m

" Search and highlighting
nnoremap <silent> <C-l> :<C-u>nohlsearch<CR><C-l>
" Move visual block
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
" Keep selection after indent
vnoremap < <gv
vnoremap > >gv
" Keep flags when repeating the search
nnoremap & :&&<CR>
xnoremap & :&&<CR>

" Leader
let mapleader = "\<Space>"
" Folding
nnoremap <Leader><Space> za
" Grep symbol under cursor
nnoremap <Leader>g :grep --<C-R>=b:current_syntax<CR> -w "<C-R><C-W>"<CR>
nnoremap <Leader>l :grep --<C-R>=b:current_syntax<CR> -l -w "<C-R><C-W>"<CR>
nnoremap <Leader>G :grep -w "<C-R><C-W>"<CR>
nnoremap <Leader>L :grep -l -w "<C-R><C-W>"<CR>
" Copy/paste
vmap <Leader>y "+y
nmap <Leader>y "+y
vmap <Leader>p "+p
nmap <Leader>p "+p
" CtrlP
nmap <Leader>f <ESC>:CtrlP<CR>
nmap <Leader>t <ESC>:CtrlPTag<CR>
" Vertical split
nnoremap <Leader>v <C-w>v<C-w>l
" Help
let b:api_search = "so"
nnoremap <Leader>h :!open http://duckduckgo.com?q="\!<C-R>=b:api_search<CR> <C-R><C-W>"<CR>

" Publish files
map <D-u> :!publish<CR>
imap <D-u> <ESC>:!publish<CR>i


augroup VIMRC
	" Remove all commands to speed up things
	autocmd!
	" Autoreload vimrc
	autocmd bufwritepost vimrc source $MYVIMRC 
	" Load template if available
	autocmd BufNewFile * silent! :execute '0r ~/.vim/templates/' . '%:e' . '.tpl'
	" Return to last edit position when opening files (You want this!)
	autocmd BufReadPost *
				\ if line("'\"") > 0 && line("'\"") <= line("$") |
				\   exe "normal! g`\"" |
				\ endif
	" Autodestroy past Git windows
	autocmd BufReadPost fugitive://* set bufhidden=delete
augroup end

" NetRW
let g:netrw_banner = 0
let g:netrw_preview = 1
let g:netrw_winsize = 25
if version > 704
	let g:netrw_list_hide = netrw_gitignore#Hide() . ',\.git/,\..*sw.'
else 
	let g:netrw_list_hide = '\.sass-cache/,\.git/,\.tags,\.DS_Store,\..*sw.'
endif
		
" Status line
set laststatus=2
set statusline=
set statusline+=%2*[%n%H%M%R%W]%*\  " flags and buf no
set statusline+=%f\ 		            " path
set statusline+=%y\        			" file type
set statusline+=%{exists('g:loaded_fugitive')?fugitive#statusline():''} " Git branch
set statusline+=%=                  " right align
set statusline+=%l\            		" line and column
set statusline+=(%P)                " percentage of file

" CtrlP
let g:ctrlp_custom_ignore = '\v(modules|images|\.git$|\.hg|\.svn|\.css)'
let g:ctrlp_cache_dir = $HOME . '/.cache/ctrlp'
let g:ctrlp_max_depth = 2

" SuperTab
let g:SuperTabLongestEnhanced = 1
let g:SuperTabLeadingSpaceCompletion = 0
let g:SuperTabClosePreviewOnPopupClose = 1
let g:SuperTabDefaultCompletionType = "context"

