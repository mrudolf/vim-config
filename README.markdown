### Vim configuration

### Cloning this configuration
```
git clone git clone https://mrudolf@bitbucket.org/mrudolf/vim-config.git .vim
cd .vim
git submodule update --init
ln -sf vimrc ../.vimrc
```

#### Add new module
```
cd ~/.vim
git submodule add http://github.com/tpope/vim-fugitive.git bundle/fugitive
git add .
git commit -m "Install Fugitive.vim bundle as a submodule."
```

#### Updating all modules
```
git submodule foreach git pull origin master
```

### Adding pulled modules in client
```
git submodule update --init
```

### Remove module
git rm bundle/*bundlename*

### Modules used
* [fugitive](http://github.com/tpope/vim-fugitive.git) – Git management
* [repeat](https://github.com/tpope/vim-repeat.git) – repeat last command, including functions
* [supertab](https://github.com/ervandew/supertab.git) – use Tab to trigger omnicomplete
* [vim-assistant](http://github.com/alvan/vim-assistant.git) – display function definition
* [vim-matchit](https://github.com/tmhedberg/matchit) – extended % matching for HTML and other languages
* [vim-surround](https://github.com/tpope/vim-surround) – surround marked text with text
* [vim-unimpaired](http://github.com/tpope/vim-unimpaired) – many navigation shortcuts (next/previous)


### Previously used models
* [snippetsemu](https://github.com/vim-scripts/snippetsEmu) – simple code snippets   
* [syntastic](https://github.com/scrooloose/syntastic.git) – syntax checking for many languages  

